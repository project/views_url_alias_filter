<?php

namespace Drupal\views_url_alias_filter\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Convert an URL alias to it's entity ID.
 *
 * @ViewsArgumentDefault(
 *   id = "views_url_alias_entity_id",
 *   title = @Translation("URL alias to entity ID")
 * )
 */
class URLAliasFilter extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * The instance of the alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected AliasManagerInterface $aliasManager;

  /**
   * The alias for which we want to get the type.
   *
   * @var string
   */
  protected $aliasValue;

  /**
   * {@inheritdoc}
   */
  public function __construct(
   array $configuration,
   $plugin_id,
   $plugin_definition,
   AliasManagerInterface $alias_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
     $configuration,
     $plugin_id,
     $plugin_definition,
     $container->get('path_alias.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['query_param'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['query_param'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Query parameter'),
      '#description' => $this->t('The query parameter to use.'),
      '#default_value' => $this->options['query_param'],
      '#required' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $current_request = $this->view->getRequest();
    if ($current_request->query->has($this->options['query_param'])) {
      $param = $current_request->query->get($this->options['query_param']);
      if ($param !== '') {
        if ($param[0] !== '/') {
          $param = '/' . $param;
        }
        // Getting the internal entity path value from path alias.
        $internal_path = $this->aliasManager->getPathByAlias($param);
        return substr($internal_path, strrpos($internal_path, '/') + 1);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['url'];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $dependencies['module'][] = 'views_url_alias_filter';
    return $dependencies;
  }

}
